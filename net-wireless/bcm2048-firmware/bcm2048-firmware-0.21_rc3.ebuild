# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=4

inherit eutils

DESCRIPTION="Binary Nokia N900 bcm2048 bluetooth firmware"
LICENSE="Nokia"
HOMEPAGE="http://maemo.org"

SRC_URI="https://coderus.openrepos.net/n900mirror/downloads.maemo.nokia.com/fremantle/ssu/210/bt-firmware_0.21-rc3%2b0m5_all.deb"
KEYWORDS="arm"
SLOT="0"
RESTRICT="mirror bindist"

IUSE=""
RDEPEND=""
DEPEND=""

S="${WORKDIR}"

src_unpack() {
	unpack "${A}"
	unpack ./data.tar.gz
	unpack ./usr/share/doc/bt-firmware/changelog.Debian.gz
}

src_prepare() {
	mv changelog.Debian changelog
}

src_install() {
	dodoc changelog
	insinto /lib/firmware
	doins lib/firmware/bcmfw.bin
}
