# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit git-2 linux-mod

DESCRIPTION="Various OMAP 3 camera firmwares"
HOMEPAGE="https://gitorious.org/omap3camera"
SRC_URI=""
EGIT_REPO_URI="git://gitorious.org/omap3camera/camera-firmware.git"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS=""
IUSE="cameras_et8ek8 cameras_mt9p012 cameras_tcm8330md cameras_vs6555"

REQUIRED_USE="
	|| ( cameras_et8ek8 cameras_mt9p012 cameras_tcm8330md cameras_vs6555 )
"
RDEPEND=""
DEPEND=""

src_prepare() {
	# remove debian-based rc TODO
	echo > Makefile
}

src_compile() {
	local obj_m
	mkdir src
	if use cameras_et8ek8; then
		./makemodes.pl -d -s et8ek8 ini/stingraytsb_v14_simple.ini > src/et8ek8-0002.c || die 'makemodes et8ek8 failed'
		obj_m+=' src/et8ek8-0002.o'
	fi
	if use cameras_mt9p012; then
		./makemodes.pl -s mt9p012 ini/stingraykmot_v06.ini > src/mt9p012-2802-10.c || die 'makemodes mt9p012 failed'
		obj_m+=' src/mt9p012-2802-10.o'
	fi
	if use cameras_tcm8330md; then
		./makemodes.pl -s tcm8330md -i Mode_648x488@15FPS_RAW10_13.0MHz,Mode_648x488@30FPS_RAW10_13.0MHz ini/acmelitetsb_v02.ini > src/smia-sensor-0c-208a-03.c || die 'makemodes tcm8330md failed'
		obj_m+=' src/smia-sensor-0c-208a-03.o'
	fi
	if use cameras_vs6555; then
		./makemodes.pl -s vs6555 -i Mode_648x488@15FPS_RAW10_13.0MHz,Mode_648x488@30FPS_RAW10_13.0MHz ini/acmelitest_v02.ini > src/smia-sensor-01-022b-00.c || die 'makemodes vs6555 failed'
		./makemodes.pl -s vs6555 -i Mode_648x488@15FPS_RAW10_13.0MHz,Mode_648x488@30FPS_RAW10_13.0MHz ini/acmelitest_v02.ini > src/smia-sensor-01-022b-04.c || die 'makemodes vs6555 failed'
		obj_m+=' src/smia-sensor-01-022b-00.o src/smia-sensor-01-022b-04.o'
	fi
	make -C "${KERNEL_DIR}" M="${PWD}" obj-m="$obj_m" LDFLAGS="$(get_abi_LDFLAGS)" modules || die 'make modules failed'
	for in in ${obj_m}; do
		out="`echo "$in" | sed 's/.o$/.bin/'`"
		objcopy -O binary $in $out || die "objcopy $out failed"
	done
}

src_install() {
	insinto "/lib/firmware"
	doins src/*.bin
	dodoc debian/changelog
}

pkg_preinst() { true; }
pkg_postinst() { true; }
pkg_postrm() { true; }
