# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI=2

IUSE="bluetooth cellular gps nokia-osso-linux v4l wifi X"
# TODO: alsa gps
LICENSE="GPL-2"
DESCRIPTION="Meta-package bringing minimal needed software to operate on Nokia N900 tablets"
HOMEPAGE="http://luke.dashjr.org/programs/gentoo-n900/"
SLOT="0"
KEYWORDS="arm"
RDEPEND="
	nokia-osso-linux? (
		sys-apps/nit-bootmenu-compat[n900]
		sys-kernel/fremantle-sources
	)
	bluetooth? (
		|| (
			net-wireless/bluez
			net-wireless/bluez-utils
		)
		net-wireless/bcm2048-firmware
	)
	cellular? (
		net-misc/ofono[n900modem]
	)
	gps? (
		net-misc/ofono[n900modem]
	)
	v4l? (
		media-video/omap3camera-firmware
	)
	wifi? (
		|| (
			net-wireless/wl1251-firmware-nokia
			net-wireless/wl1251-firmware
		)
		net-wireless/wireless-tools
	)
	X? (
		x11-base/xorg-server[tslib]
		|| (
			x11-base/xorg-server[input_devices_evdev,input_devices_tslib]
			x11-base/xorg-drivers[input_devices_evdev,input_devices_tslib]
		)
		|| (
			x11-base/xorg-server[video_cards_fbdev]
			x11-base/xorg-drivers[video_cards_fbdev]
			x11-drivers/xf86-video-omapfb
		)
		x11-misc/xkb-nokia
	)
"
