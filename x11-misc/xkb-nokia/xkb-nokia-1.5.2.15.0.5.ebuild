# Distributed under the terms of the GNU General Public License v2
# $Header: $

DESCRIPTION="XKB data files for Nokia Internet Tablets"
HOMEPAGE="http://maemo.org"
LICENSE="MIT"
SLOT="0"

MY_PN="xkeyboard-config"
MY_PV="1.5-2maemo15+0m5"
SRC_URI="http://repository.maemo.org/pool/maemo5.0/free/x/xkeyboard-config/${MY_PN}_${MY_PV}.tar.gz"
KEYWORDS="~arm"
IUSE=""
RDEPEND="
	x11-misc/xkeyboard-config
"

S="${WORKDIR}/${MY_PN}-1.5"

src_compile() {
	./autogen.sh || die 'autogen failed'
	cd rules
	make
}

src_install() {
	insinto /usr/share/X11/xkb/symbols/nokia_vndr
	for sym in rx-44 rx-51 su-8w; do
		doins symbols/nokia_vndr/$sym
	done
	insinto /usr/share/X11/xkb/types
	doins types/nokia
	insinto /usr/share/X11/xkb/geometry
	doins geometry/nokia
	insinto /usr/share/X11/xkb/rules
	for ext in '' .xml .lst; do
		newins rules/base$ext nokia$ext
	done
}
