# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-misc/ofono/ofono-0.25.ebuild,v 1.1 2010/07/28 11:57:29 dagger Exp $

EAPI="2"

inherit git multilib

DESCRIPTION="Open Source mobile telephony (GSM/UMTS) daemon."
HOMEPAGE="http://ofono.org/"
SRC_URI=""
EGIT_REPO_URI="git://git.kernel.org/pub/scm/network/${PN}/${PN}.git"
EGIT_PROJECT="${P}"
EGIT_OFFLINE=1
EGIT_BOOTSTRAP='bootstrap'
EGIT_COMMIT='30d284a73f03334e96d592874a38abffa6afdc2f'

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~arm ~amd64 ~x86"
IUSE="atmodem bluetooth +caps examples +isimodem +n900modem +network threads +udev"

RDEPEND=">=sys-apps/dbus-1.2.24
	bluetooth? ( >=net-wireless/bluez-4.61 )
	caps? ( sys-libs/libcap-ng )
	>=dev-libs/glib-2.16
	network? (
		dev-lang/perl
		dev-python/dbus-python
	)
	udev? ( >=sys-fs/udev-143 )
	examples? ( dev-python/dbus-python )"

DEPEND="${RDEPEND}
	dev-util/pkgconfig"

RDEPEND="${RDEPEND}
	n900modem? ( =${CATEGORY}/${PF}[isimodem] )
"

src_prepare() {
	use n900modem &&
		epatch "${FILESDIR}/ofono-0.27_pre-n900modem.patch"
	git_src_prepare
}

src_configure() {
	econf \
		$(use_enable caps capng) \
		$(use_enable threads) \
		$(use_enable udev) \
		$(use_enable isimodem) \
		$(use_enable atmodem) \
		--enable-test \
		--localstatedir=/var
}

src_install() {
	emake DESTDIR="${D}" install || die "emake install failed"

	if ! use examples ; then
		rm -rf ${D}/usr/"$(get_libdir)"/ofono/test
	fi

	newinitd "${FILESDIR}"/${PN}.initd ${PN} || die
	dodoc ChangeLog AUTHORS doc/*.txt

	if use network; then
		insinto /lib/rc/net
		newins "${FILESDIR}/${PN}.rcnet" "${PN}.sh"
	fi
}
