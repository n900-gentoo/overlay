# Copyright 1999-2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/net-misc/ofono/ofono-0.51.ebuild,v 1.1 2011/07/01 15:23:08 dagger Exp $

EAPI="4"

inherit git-2 eutils multilib systemd

DESCRIPTION="Open Source mobile telephony (GSM/UMTS) daemon."
HOMEPAGE="http://ofono.org/"
SRC_URI=""
EGIT_REPO_URI="git://git.kernel.org/pub/scm/network/${PN}/${PN}.git"
EGIT_PROJECT="${P}"
EGIT_COMMIT='ef75a44c12441ea27a906f001083c3c1450af976'

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~arm ~amd64 ~x86"
IUSE="+atmodem +blocking-examples bluetooth +caps +cdmamodem +examples +isimodem +mbpi +n900modem +network +phonesim threads +tools +udev"

OFONO_TOOLS="auto-enable get-location huawei-audio"

REQUIRED_USE="
	blocking-examples? ( examples )
	n900modem? ( isimodem )
"
RDEPEND="
	>=sys-apps/dbus-1.2.24
	>=dev-libs/glib-2.16
	bluetooth? ( >=net-wireless/bluez-4.61 )
	caps? ( sys-libs/libcap-ng )
	tools? ( dev-libs/libusb:1 )
"
DEPEND="${RDEPEND}
	dev-util/pkgconfig
"
RDEPEND="${RDEPEND}
	blocking-examples? ( dev-python/dbus-python )
	mbpi? ( net-misc/mobile-broadband-provider-info )
	udev? ( >=sys-fs/udev-143 )
"

src_prepare() {
	epatch "${FILESDIR}/Bugfix-Can-t-build-telit-module-without-bluetooth-so.patch"
	use n900modem &&
	epatch "${FILESDIR}/udev-rule-to-match-Nokia-N900-with-stock-2.6.28-kern.patch"
	./bootstrap || die
}

src_configure() {
	econf \
		$(use_enable caps capng) \
		$(use_enable threads) \
		$(use_enable udev) \
		$(use_enable isimodem) \
		$(use_enable atmodem) \
		$(use_enable cdmamodem) \
		$(use_enable bluetooth) \
		$(use_enable phonesim) \
		$(use_enable tools) \
		--enable-test \
		--localstatedir=/var \
		--with-systemdunitdir="$(systemd_get_unitdir)"
}

src_install() {
	local Dlib="${D}/usr/$(get_libdir)/ofono"
	emake DESTDIR="${D}" install

	newinitd "${FILESDIR}"/${PN}.initd ${PN}
	dodoc ChangeLog AUTHORS doc/*.txt

	if use network; then
		insinto /lib/rc/net
		newins "${FILESDIR}/${P}.rcnet" "${PN}.sh"
	fi

	if use udev ; then
		insinto /etc/udev/rules.d
		newins plugins/ofono.rules 72-ofono.rules
	fi

	if use tools ; then
		cd tools || die
		for e in ${OFONO_TOOLS} ; do
			newbin {,ofono-}"$e"
		done
		if use mbpi ; then
			newbin {,ofono-}lookup-apn
		fi
	fi

	if use examples ; then
		cd "${Dlib}/test" || die
		if ! use blocking-examples ; then
			grep -l glib * | xargs rm
		fi
		for e in * ; do
			newbin {,ofono-}"$e"
		done
	fi
	rm -rf "${Dlib}/test"
	rmdir "${Dlib}"
}
