# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

EAPI="2"
K_SECURITY_UNSUPPORTED=1
K_NOSETEXTRAVERSION=1
ETYPE="sources"
K_DEBLOB_AVAILABLE="0"
inherit eutils versionator

MY_PN="kernel"
CKV="$(get_version_component_range 1-3 ${PV})"
MAEMOVERSION="20103103+0m5"
MY_KV="${CKV}-${MAEMOVERSION}"
MY_P="${MY_PN}_${MY_KV}"
MY_PATCH="${MY_P}.diff.gz"
UKV="$(get_version_component_range 1-4 ${PV})"
MY_UPDATE_PATCH="fremantle-${UKV}-${MAEMOVERSION}-r1.patch.xz"
UNIPATCH_LIST_DEFAULT="
	${DISTDIR}/${MY_PATCH}
	${DISTDIR}/${MY_UPDATE_PATCH}
	${FILESDIR}/${MY_KV}_gcc44.patch
	${FILESDIR}/kbuild-fix-make-incompatibility.patch
	${FILESDIR}/Bugfix-enable-sec-extension-for-omap3-rom-asm.patch
	${FILESDIR}/no-crosscompile.patch
	${FILESDIR}/${MY_KV}_0001-ARM-5884-1-arm-Fix-DCC-console-for-v7.patch
	${FILESDIR}/armthumb.patch
	${FILESDIR}/bq27x00_rx51_hack.patch
	${FILESDIR}/gentoo-fsfixes.diff
	${FILESDIR}/gethercharge.diff
	${FILESDIR}/wl1251-monitor-mode.diff
	${FILESDIR}/wl12xx_rohar.diff
"
UNIPATCH_STRICTORDER=true
inherit kernel-2
detect_version
EXTRAVERSION="-fremantle${MAEMOVERSION}"
KV="${UKV}${EXTRAVERSION}"
MyPR="-${PR}"
[ "${MyPR}" = '-r0' ] && MyPR=
KV_FULL="${KV}${MyPR}"
S="${WORKDIR}/linux-${KV_FULL}"

SRC_DIR="http://repository.maemo.org/pool/fremantle/free/k/${MY_PN}"

DESCRIPTION="Maemo 5 kernel based on Linux"
HOMEPAGE="http://www.maemo.org"
SRC_URI="${KERNEL_URI}
	${SRC_DIR}/${MY_PATCH}
	http://luke.dashjr.org/programs/gentoo-n900/kernels/patches/${MY_UPDATE_PATCH}
"

KEYWORDS="~arm"
IUSE="fcam"

src_unpack() {
	if use fcam; then
		UNIPATCH_LIST_DEFAULT="$UNIPATCH_LIST_DEFAULT
			${FILESDIR}/${MY_KV}_fcam-1.0.7-1.patch
		"
	fi
	kernel-2_src_unpack
	cd "${S}"
	rm -r .gitignore .mailmap debian
}

pkg_postinst() {
	if use fcam; then
		einfo "Fcam requires setting sysctl kernel.sched_rt_runtime_us to -1"
		einfo "To do this now, run: sysctl -w kernel.sched_rt_runtime_us=-1"
		einfo "To set it at boot, append /etc/sysctl.conf with:"
		einfo "    kernel.sched_rt_runtime_us = -1"
	fi
}
