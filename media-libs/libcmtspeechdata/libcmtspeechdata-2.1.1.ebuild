# Copyright 2011 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI="4"

inherit autotools-utils git-2

DESCRIPTION="Library for cellular speech data path"
HOMEPAGE="https://meego.gitorious.org/meego-cellular"
SRC_URI=""
EGIT_REPO_URI="git://gitorious.org/meego-cellular/${PN}.git"
EGIT_COMMIT='845034568692d289573e18163d497fce6177cffb'

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~arm"
IUSE="debug doc"

RDEPEND="
	>=dev-libs/check-0.9.4
	>=sys-apps/dbus-1.0
"
DEPEND="${RDEPEND}
	doc? ( app-doc/doxygen )
	dev-util/pkgconfig
"

src_prepare() {
	eautoreconf
}

src_configure() {
	local myeconfargs=(
		$(use_enable debug extra-debug)
		$(use_enable doc docs)
		--enable-nokiamodem-vdd2lock
		--disable-static
	)
	autotools-utils_src_configure
}
